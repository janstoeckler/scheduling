<?php

namespace Drupal\scheduling\StackMiddleware;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Core\Site\Settings;
use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Executes the page caching before the main kernel takes over the request.
 */
class PageCacheExtension implements HttpKernelInterface {

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {


    $response = $this->httpKernel->handle();
    // Only allow page caching on master request.
    if ($type === static::MASTER_REQUEST && $this->requestPolicy->check($request) === RequestPolicyInterface::ALLOW) {
      $response = $this->lookup($request, $type, $catch);

      if ($response->headers->get('Expires') !== 'Sun, 19 Nov 1978 05:00:00 GMT') {
        $expires = DrupalDateTime::createFromFormat('D, d M Y H:i:s \G\M\T', $response->headers->get('Expires'));
        $now = new DrupalDateTime();

        $expires_in = $expires->getTimestamp() - $now->getTimestamp();

        if ($response->headers->hasCacheControlDirective('max-age')) {
          $response->headers->addCacheControlDirective('max-age', $expires_in);
        }
      }
    } else {
      $response = $this->pass($request, $type, $catch);
    }

    return $response;
  }

}
