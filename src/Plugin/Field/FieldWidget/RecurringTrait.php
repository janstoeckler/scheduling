<?php

namespace Drupal\scheduling\Plugin\Field\FieldWidget;


use Drupal\Core\Datetime\DrupalDateTime;

trait RecurringTrait {

  public function recurringElement($values, $id, $wrapper, $mode) {
    // Range scheduling widget
    $element = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="' . $wrapper . '-recurring">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':input[name="' . $mode . '"]' => ['value' => 'recurring'],
        ],
      ],
      'entries' => [
        '#type' => 'container'
      ],
    ];
    if (count($values['entries']) < 1) {
      $values['entries'][] = [
        'from' => NULL,
        'to' => NULL,
        'weekdays' => [],
      ];
    }
    foreach ($values['entries'] as $key => $recurring) {
      $element['entries'][$key] = $this->buildRecurringWidget($recurring);
    }
    $element['add_more'] = $this->buildAddMoreButton($id, $wrapper, 'recurring');
    return $element;
  }

  protected function buildRecurringWidget(
    $value
  ) {

    foreach (['from', 'to'] as $field) {
      if (isset($value[$field])) {
        if (!($value[$field] instanceof DrupalDateTime)) {
          $value[$field] = new DrupalDateTime($value[$field]);
        }
      }
      else {
        $value[$field] = NULL;
      }
    }

    $options = [
      'Su' => $this->t('Su'),
      'Mo' => $this->t('Mo'),
      'Tu' => $this->t('Tu'),
      'We' => $this->t('We'),
      'Th' => $this->t('Th'),
      'Fr' => $this->t('Fr'),
      'Sa' => $this->t('Sa'),
    ];

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'recurring',
          'row',
        ],
      ],
      'weekdays' => [
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $options,
        '#default_value' => isset($value['weekdays']) ? $value['weekdays'] : [],
      ],
      'from' => [
        '#type' => 'datetime',
        '#date_date_element' => 'none',
        '#date_time_element' => 'time',
        '#default_value' => $value['from'],
      ],
      'to' => [
        '#type' => 'datetime',
        '#date_date_element' => 'none',
        '#date_time_element' => 'time',
        '#default_value' => $value['to'],
      ],
    ];
  }

}