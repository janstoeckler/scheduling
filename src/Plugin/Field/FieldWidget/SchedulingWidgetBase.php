<?php

namespace Drupal\scheduling\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

abstract class SchedulingWidgetBase extends WidgetBase {

  use RangeTrait;
  use RecurringTrait;

  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    $element['value'] = [
      '#tree' => TRUE,
      '#attached' => [
        'library' => [
          'scheduling/scheduling',
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public
  function massageFormValues(
    array $values,
    array $form,
    FormStateInterface $form_state
  ) {

    // The widget form element type has transformed the value to a
    // DrupalDateTime object at this point. We need to convert it back to the
    // storage timezone and format.
    foreach ($this->modes as $mode) {
      foreach ($values['value'][$mode]['entries'] as $key => $value) {
        $values['value'][$mode]['entries'][$key]['from'] = isset($values['value'][$mode]['entries'][$key]['from']) ? $values['value'][$mode]['entries'][$key]['from']->format('c') : NULL;
        $values['value'][$mode]['entries'][$key]['to'] = isset($values['value'][$mode]['entries'][$key]['to']) ? $values['value'][$mode]['entries'][$key]['to']->format('c') : NULL;
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    if ($key_exists) {

      $values = $this->removeAddMoreButton($values);

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values.
      $items->setValue($values);
    }
  }

  protected function buildAddMoreButton($id, $wrapper, $mode) {
    return [
      '#type' => 'submit',
      '#name' => strtr($id, '-', '_') . '_add_more_' . $mode,
      '#value' => t('Add'),
      '#attributes' => [
        'class' => [
          'field-add-more-submit',
        ],
      ],
      '#submit' => [
        [
          static::class, 'addMoreSubmit',
        ],
      ],
      '#ajax' => [
        'callback' => [
          static::class, 'addMoreAjax',
        ],
        'wrapper' => $wrapper . '-' . $mode,
        'effect' => 'fade',
      ],
    ];
  }

  public static function addMoreSubmit(
    array $form,
    FormStateInterface $form_state
  ) {
    $button = $form_state->getTriggeringElement();
    $button_name = explode('_', $button['#name']);
    $mode = end($button_name);

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Increment the items count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['increment'] = $mode;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

  public static function addMoreAjax(
    array $form,
    FormStateInterface $form_state
  ) {
    $button = $form_state->getTriggeringElement();

    // Go three levels up in the form, to the widgets container.
    $element = NestedArray::getValue($form,
      array_slice($button['#array_parents'], 0, -1));

    return $element;
  }

  /**
   * @param $values
   */
  protected function removeAddMoreButton($values) {
    foreach ($this->modes as $mode) {
      unset($values['value'][$mode]['add_more']);
    }
    return $values;
  }

  public static function defaultValues() {
    return ['value' => [
      'mode' => 'published',
      'range' => [
        'entries' => [
          0 => [
            'from' => NULL,
            'to' => NULL,
          ],
        ],
      ],
      'recurring' => [
        'entries' => [
          0 => [
            'weekdays' => [],
            'from' => NULL,
            'to' => NULL,
          ],
        ],
      ],
      'complex' => [
        'entries' => [
          0 => [
            'range' => [
              'from' => NULL,
              'to' => NULL,
            ],
            'recurring' => [
              'weekdays' => [],
              'from' => NULL,
              'to' => NULL,
            ],
          ],
        ],
      ],
    ]];
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $field_name
   *
   * @return array
   */
  protected function getValues(
    FieldItemListInterface $items,
    array &$form,
    FormStateInterface $form_state,
    $field_name
  ) {
    $values = !empty($items->getValue()) ? $items->getValue()[0] : static::defaultValues();

    if ($form_state->isRebuilding()) {
      // Extract the values from $form_state->getValues().
      $path = array_merge($form['#parents'], [$field_name]);
      $key_exists = NULL;
      $form_state_values = NestedArray::getValue($form_state->getValues(),
        $path, $key_exists);

      if ($key_exists) {

        // Override $values with current form state values.
        $values = $this->removeAddMoreButton($form_state_values);

        // Add possible entry increment from ajax submission.
        $field_state = static::getWidgetState($form['#parents'], $field_name,
          $form_state);
        if (isset($field_state['increment'])) {
          $increment = $field_state['increment'];
          unset($field_state['increment']);
          static::setWidgetState($form['#parents'], $field_name, $form_state,
            $field_state);
          $values['value'][$increment]['entries'][] = static::defaultValues()['value'][$increment]['entries'][0];
        }

      }
    }
    return $values;
  }

}
