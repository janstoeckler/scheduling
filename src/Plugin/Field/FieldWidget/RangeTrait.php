<?php

namespace Drupal\scheduling\Plugin\Field\FieldWidget;


use Drupal\Core\Datetime\DrupalDateTime;

trait RangeTrait {

  public function rangeElement($values, $id, $wrapper, $mode) {
    // Range scheduling widget
    $element = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="' . $wrapper . '-range">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':input[name="' . $mode . '"]' => ['value' => 'range'],
        ],
      ],
      'entries' => [
        '#type' => 'container'
      ],
    ];
    if (count($values['entries']) < 1) {
      $values['entries'][] = [
        'from' => NULL,
        'to' => NULL,
      ];
    }
    foreach ($values['entries'] as $key => $range) {
      $element['entries'][$key] = $this->buildRangeWidget($range);
    }
    $element['add_more'] = $this->buildAddMoreButton($id, $wrapper, 'range');
    return $element;
  }

  protected
  function buildRangeWidget(
    $value
  ) {

    foreach (['from', 'to'] as $field) {
      if (isset($value[$field])) {
        if (!($value[$field] instanceof DrupalDateTime)) {
          $value[$field] = new DrupalDateTime($value[$field]);
        }
      } else {
        $value[$field] = NULL;
      }
    }

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'range',
          'row',
        ],
      ],
      'from' => [
        '#type' => 'datetime',
        '#default_value' => $value['from'],
      ],
      'to' => [
        '#type' => 'datetime',
        '#default_value' => $value['to'],
      ],
    ];
  }

}