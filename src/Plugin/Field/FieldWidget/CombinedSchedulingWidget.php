<?php

namespace Drupal\scheduling\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'scheduling' widget.
 *
 * @FieldWidget(
 *   id = "scheduling_combined",
 *   label = @Translation("Combined scheduling"),
 *   field_types = {
 *     "scheduling"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CombinedSchedulingWidget extends SchedulingWidgetBase {

  use RangeTrait;
  use RecurringTrait;

  protected $modes = [
    'range',
    'recurring'
  ];

  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Get states & build identifiers.
    $field_name = $this->fieldDefinition->getName();
    $id = implode('-', array_merge($form['#parents'], [$field_name]));
    $wrapper = Html::getUniqueId($id . '-add-more');

    // Get values from field and/or form state.
    $values = $this->getValues($items, $form, $form_state, $field_name);

    $mode_parents = array_merge($form['#parents'], ['scheduling', 'value', 'mode']);
    $mode = reset($mode_parents) . '[' . implode('][', array_slice($mode_parents, 1)) . ']';

    // Scheduling mode selection.
    $element['value']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Scheduling'),
      '#default_value' => isset($values['value']['mode']) ? $values['value']['mode'] : 'published',
      '#options' => [
        'published' => $this->t('Published'),
        'range' => $this->t('Range'),
        'recurring' => $this->t('Recurring'),
      ],
    ];

    // Range scheduling widget.
    $element['value']['range'] = $this->rangeElement($values['value']['range'], $id, $wrapper, $mode);

    // Recurring scheduling widget.
    $element['value']['recurring'] = $this->recurringElement($values['value']['recurring'], $id, $wrapper, $mode);

    return $element;
  }

}
