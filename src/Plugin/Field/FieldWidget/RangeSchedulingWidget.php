<?php

namespace Drupal\scheduling\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'scheduling' widget.
 *
 * @FieldWidget(
 *   id = "scheduling_range",
 *   label = @Translation("Range scheduling"),
 *   field_types = {
 *     "scheduling"
 *   },
 *   multiple_values = TRUE
 * )
 */
class RangeSchedulingWidget extends SchedulingWidgetBase {

  use RangeTrait;

  protected $modes = [
    'range',
  ];

  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Get states & build identifiers.
    $field_name = $this->fieldDefinition->getName();
    $id = implode('-', array_merge($form['#parents'], [$field_name]));
    $wrapper = Html::getUniqueId($id . '-add-more');

    // Get values from field and/or form state.
    $values = $this->getValues($items, $form, $form_state, $field_name);

    // Scheduling mode selection.
    $element['value']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Scheduling'),
      '#default_value' => isset($values['value']['mode']) ? $values['value']['mode'] : 'published',
      '#options' => [
        'published' => $this->t('Published'),
        'range' => $this->t('Range'),
      ],
    ];

    // Range scheduling widget.
    $element['value']['range'] = $this->rangeElement($values['value']['range'], $id, $wrapper);

    return $element;
  }

}
